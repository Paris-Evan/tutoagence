@extends('admin.admin')

@section('title','tous nos biens')

@section('content')
    <div class="d-flex justify-content-betwen align-items-center">
        <h1>@yield('title')</h1>
    </div>

    <a href="{{route('admin.property.create')}}" class="btn btn-primary">Ajouter un bien</a>

    <table class="table table-striped" >
        <thead>
        <tr>
            <th>Titre</th>
            <th>Surface</th>
            <th>Prix</th>
            <th>Ville</th>
            <th class='texte-end'>Action</th>
        </tr>
        </thead>

        <tbody>
        @foreach($properties as $property)
            <tr>
                <td>{{$property->title}}</td>
                <td>{{$property->surface}}m2</td>
                <td>{{number_format($property->price,thousands_separator: '')}}</td>
                <td>{{$property->city}}</td>
                <td>

                </td>
            </tr>

        @endforeach
        </tbody>

    </table>
    {{$properties->links()}}

@endsection
